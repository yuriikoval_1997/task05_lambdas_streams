package sub_task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Receiver {
    public void start(){
        System.out.println("The receiver has started");
    }

    public void stop(){
        System.out.println("The receiver has stopped");
    }

    public void LiftUpReceiver(){
        System.out.println("Lift up the receiver, I'll make you a believer.");
    }

    public void enterYourMessage() throws IOException {
        String message;
        try(BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in))) {
            message = consoleReader.readLine();
        }
        System.out.printf("You entered: \"%s\"", message);
        System.out.println();
    }
}
