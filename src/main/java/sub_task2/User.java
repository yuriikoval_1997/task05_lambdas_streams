package sub_task2;

import functional_interfaces.Command;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class User {
    private Command startCommand;
    private Command stopCommand;
    private Command liftUpCommand;
    private Command enterMessageCommand;

    public User(Command startCommand, Command stopCommand, Command liftUpCommand, Command enterMessageCommand) {
        this.startCommand = startCommand;
        this.stopCommand = stopCommand;
        this.liftUpCommand = liftUpCommand;
        this.enterMessageCommand = enterMessageCommand;
    }

    public void askAboutCommand() throws Exception{
        System.out.println("Enter one of the next command: start, stop, lift, enter");
        String commandMessage;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))){
            while (true){
                commandMessage = bufferedReader.readLine().toLowerCase();
                switch (commandMessage){
                    case "start":
                        startCommand.execute();
                        return;
                    case "stop":
                        stopCommand.execute();
                        return;
                    case "lift":
                        liftUpCommand.execute();
                        return;
                    case "enter":
                        enterMessageCommand.execute();
                        return;
                    default:
                        System.out.println("Oops, wrong command. Try again!");
                }
            }
        }
    }
}
