package sub_task3;

import java.util.ArrayList;
import java.util.Random;

public class PseudoDataGenerator {
    /**
     * This method creates a ArrayList and fills it with int pseudo data.
     * @return ArrayList of pseudo data.
     */
    public static ArrayList<Integer> generatePseudoData(){
        final int upperBound = 100;
        final int size = 100;
        Random random = new Random();
        ArrayList<Integer> pseudoData = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            pseudoData.add(random.nextInt(upperBound));
        }
        return pseudoData;
    }
}
