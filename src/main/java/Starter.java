import functional_interfaces.Command;
import functional_interfaces.IntConsumer;
import sub_task2.Receiver;
import sub_task2.StartCommand;
import sub_task2.User;
import sub_task3.PseudoDataGenerator;
import sub_task4.TextParser;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Starter {
    public static void main(String[] args) throws Exception {
        testFirstSubTask();
        testSecondSubTask();
        testThirdSubTask();
        testForthSubTask();
    }

    /**
     * This method sorts and prints appearance of words in the file.
     * @throws IOException if TextParser.readFromFile(...) throws one
     */
    private static void testForthSubTask() throws IOException {
        final String fileName = "text/that_is_life.txt";
        final TreeMap<String, Integer> wordAppearance_e1 = new TreeMap<>();
        final TreeMap<String, Integer> wordAppearance_e2 = new TreeMap<>();
        List<String> lines = TextParser.readFromFile(fileName);

        // Using map(), which returns Stream<changed elements>. In this case String<String[]>.
        lines.stream()
                .map(line -> line.split("\\W"))
                .forEach(line -> {
                    List<String> words = Arrays.asList(line);
                    words.forEach(word -> wordAppearance_e1.merge(word, 1, Integer::sum));
                });

        // Using flatMap(), which returns Stream<changed elements>. In this case Stream<Stream<String>>
        lines.stream()
                .flatMap(line -> Arrays.stream(line.split("\\W")))
                .forEach(word -> wordAppearance_e2.merge(word, 1, Integer::sum));
        wordAppearance_e1.forEach((k, v) -> System.out.println(k + " " + v));
        System.out.println("If two maps are the same? - " + wordAppearance_e1.equals(wordAppearance_e2));
    }

    /**
     * Process pseudo data using Stream API, IntSummaryStatistics
     */
    private static void testThirdSubTask(){
        ArrayList<Integer> pseudoData = PseudoDataGenerator.generatePseudoData();
        final int average = (int) pseudoData.stream()
                .mapToInt(Integer::intValue)
                .average()
                .orElseThrow(IllegalArgumentException::new);
        List<Integer> biggerThanAverage = pseudoData.stream()
                .filter(e -> e > average)
                .collect(Collectors.toList());
        System.out.println("Elements which are bigger that average: ");
        biggerThanAverage.forEach(e -> System.out.print(e + " "));
        System.out.println();
        IntSummaryStatistics intSummaryStatistics = pseudoData.stream()
                .mapToInt(Integer::intValue)
                .summaryStatistics();
        System.out.println(intSummaryStatistics);
    }

    /**
     * Test command pattern
     * @throws Exception if BufferedRead throws one
     */
    private static void testSecondSubTask() throws Exception {
        final Receiver receiver = new Receiver();
        Command startCommand = new StartCommand(receiver);
        Command stopCommand = new Command() {
            @Override
            public void execute() {
                receiver.stop();
            }
        };
        Command liftUpCommand = () -> receiver.LiftUpReceiver();
        Command enterMessageCommand = receiver::enterYourMessage;
        User user = new User(startCommand, stopCommand, liftUpCommand, enterMessageCommand);
        user.askAboutCommand();
    }

    /**
     * This method creates an array and fills it with int pseudo data.
     * After that the method prints max and average number of the data using Stream API.
     */
    private static void testFirstSubTask(){
        final int upperBound = 100;
        final int size = 100;
        Random random = new Random();
        int[] pseudoData = new int[size];
        for (int i = 0; i < pseudoData.length; i++) {
            pseudoData[i] = random.nextInt(upperBound);
        }
        IntConsumer maxCalculator = (data) -> Arrays.stream(data)
                .max()
                .orElseThrow(IllegalArgumentException::new);
        IntConsumer averageCalculator = (data) -> (int) Arrays.stream(data)
                .average()
                .orElseThrow(IllegalArgumentException::new);
        System.out.println(maxCalculator.calculateStatistics(pseudoData));
        System.out.println(averageCalculator.calculateStatistics(pseudoData));
        try {
            pseudoData = new  int[0];
            System.out.println(maxCalculator.calculateStatistics(pseudoData));
        } catch (IllegalArgumentException e){
            System.out.println("Exception has been caught. Array is empty");
        }
    }
}
