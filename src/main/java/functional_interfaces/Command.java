package functional_interfaces;

@FunctionalInterface
public interface Command {
    /**
     * This is created for invoking other methods.
     * @throws Exception according to implementation.
     */
    void execute() throws Exception;
}
