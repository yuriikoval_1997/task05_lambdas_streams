package functional_interfaces;

@FunctionalInterface
public interface IntConsumer {
    /**
     *
     * @param n array of data
     * @return calculation
     */
    int calculateStatistics(int... n);
}
