package sub_task4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class TextParser {
    /**
     * This method reads all lines from a file into List and returns it.
     * @param fileName the path to the file
     * @return List of Strings (lines from the file)
     * @throws IOException if File.readAllLines(...) throws one.
     */
    public static List<String> readFromFile(String fileName) throws IOException {
        return Files.readAllLines(Paths.get(fileName));
    }
}
